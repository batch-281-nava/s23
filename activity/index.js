// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = {
    Name: 'Ash Ketchum',
    Age: 10,
    Pokemon: ['Bulbasaur', 'Charmander', 'Totodile', 'Bayleef'],
    Friends: {Hoenn: ['Max', 'May'], Kanto: ['Brock', 'Misty'] },
    talk: function(){
        console.log("Bayleef! I choose you!")
    }
}
console.log(trainer);


// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object


let pokemonTrainer = { 
    name: "Ash Kethchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {hoenn: ["Mary", "Max"], kanto: ["Brock", "Misty"]},
    talk: function(){
        console.log('Pikachu! I choose you!')
    }
}



console.log(typeof pokemonTrainer);



/* 6. Access the trainer object properties using dot and square bracket notation.
*/
console.log('Result of dot notation: ');
console.log(trainer.name);
console.log('Result of square bracket notation: ');
console.log(trainer["pokemon"]);

 /*7. Invoke/call the trainer talk object method.*/
console.log('Result of talk method')
trainer.talk();
 
 /*8. Create a constructor for creating a pokemon with the following properties:
 - Name (Provided as an argument to the contructor)
 - Level (Provided as an argument to the contructor)
 - Health (Create an equation that uses the level property)
 - Attack (Create an equation that uses the level property)
*/
 function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.tackle = function(target) {
        let _targetPokemonHealth_ = target.health - this.attack;   
    console.log(this.name + ' tackled ' + target.name);
    console.log(target.name + " health is now reduced to " + _targetPokemonHealth_);
        target.health = _targetPokemonHealth_;
        if (_targetPokemonHealth_ <= 0){
            this.faint(target);
            
      }  
       
    }

         this.faint = function(target) {
             console.log(target.name + ' fainted.');
    }
}

 /*9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
*/
let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon("Geodude", 8, 16, 8);
let meotwo = new Pokemon("Meotwo", 100, 200, 100);






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}